FROM nginx:1.21.4-alpine

COPY . /usr/share/nginx/www
COPY nginx.conf /etc/nginx/conf.d/default.conf
RUN apk update && apk upgrade
RUN apk add php7 php7-fpm php7-opcache php7-mysqli php7-ctype php7-fileinfo php7-json php7-mbstring php7-iconv php7-zlib php7-curl php7-gd php7-simplexml php7-intl php7-ldap php7-apcu php7-xmlrpc php7-exif php7-zip php7-bz2 php7-sodium sed
